﻿using System;

namespace Calculator
{
    // Inheriting from Calculation class
    public class LoanAmountCalculation : Calculation, ICalculatable
    {
        public new double LoanAmount { get; private set; }

        public void Calculate()
        {
            // Local helper vars
            double _monthlyInterestRate;
            int _numMonthlyPayments;

            // Calculate the helper vars with helper methods
            _numMonthlyPayments = NumberOfMonthlyPayments(IsLoanTermMonthly, LoanTerm);
            _monthlyInterestRate = CalculateMonthlyRate(InterestRate);

            // Calculate Mothly payment
            LoanAmount = Math.Round(CalculateLoanAmount(_monthlyInterestRate, MonthlyPayment, _numMonthlyPayments), 2);
        }

        public double Result()
        {
            return LoanAmount;
        }
    }
}
