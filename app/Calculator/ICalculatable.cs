﻿namespace Calculator
{
    interface ICalculatable
    {

        // Methods we will need for any calculation
        // Returns result
        double Result();

        // Does calculation
        void Calculate();

    }
}
