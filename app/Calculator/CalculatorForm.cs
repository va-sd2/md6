﻿using System;
using System.Windows.Forms;

namespace Calculator
{
    public partial class CalculatorForm : Form
    {
        // Constants
        private readonly string[] CalcTypes = { "Payment Amount (€)", "Loan Amount (€)", "Loan Term", "Interest rate (%)" };
        private readonly string[] LoanTermTypes = { "Years", "Months" };
        // Generate example constants
        private const int LoanAmount = 1000;
        private const int LoanTerm = 5;
        private const int InterestRatePrcPerYear = 7;
        private const int MonthlyPayment = 200;
        //
        int selectedCalcTypeIndex;

        MonthlyPaymentCalculation monthlyPaymentCalculation = new MonthlyPaymentCalculation();
        LoanAmountCalculation loanAmountCalculation = new LoanAmountCalculation();
        LoanTermCalculation loanTermCalculation = new LoanTermCalculation();
        InterestRateCalculation interestRateCalculation = new InterestRateCalculation();

        public CalculatorForm()
        {
            InitializeComponent();
            // Assign the loan term combo box values
            cboLoanTermUnit.Items.AddRange(LoanTermTypes);
            cboLoanTermUnit.SelectedIndex = 0;
            // Assign the calc type combo box values
            cboCalculationType.Items.AddRange(CalcTypes);
            cboCalculationType.SelectedIndex = 0;
            // Update the global var
            selectedCalcTypeIndex = cboCalculationType.SelectedIndex;
            // Reset all fields
            resetCalculator(btnReset, new EventArgs());
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            // Check which calculation is to be done
            switch(selectedCalcTypeIndex)
            {
                // Payment Amount (€)
                case 0:
                    {
                        // Calculate
                        monthlyPaymentCalculation.Calculate();
                        // Fill in the appropriate text box
                        txtMonthlyPayment.Text = monthlyPaymentCalculation.Result().ToString();
                        break;
                    }
                // Loan Amount (€)
                case 1:
                    {
                        // Calculate
                        loanAmountCalculation.Calculate();
                        // Fill in the appropriate text box
                        txtLoanAmount.Text = loanAmountCalculation.Result().ToString();
                        break;
                    }
                // Loan Term
                case 2:
                    {
                        // Calculate
                        loanTermCalculation.Calculate();
                        // Fill in the appropriate text box
                        txtLoanTerm.Text = loanTermCalculation.Result().ToString();
                        break;
                    }
                // Interest rate (%)
                case 3:
                    {
                        // Calculate
                        interestRateCalculation.Calculate();
                        // Fill in the appropriate text box
                        txtInterestRate.Text = interestRateCalculation.Result().ToString();
                        break;
                    }
            }
        }

        private void resetCalculator(object sender, EventArgs e)
        {
            // Reset all input fields
            txtMonthlyPayment.Text = "";
            txtLoanAmount.Text = "";
            txtLoanTerm.Text = "";
            txtInterestRate.Text = "";
            // Reset Loan Term Unit to Yearly
            cboLoanTermUnit.SelectedIndex = 0;
            // Disable Calculation and Solution buttons
            btnCalculate.Enabled = false;
        }

        private void btnGenerateExample_Click(object sender, EventArgs e)
        {
            // Fill in the text boxes with constants
            txtLoanAmount.Text = LoanAmount.ToString();
            txtLoanTerm.Text = LoanTerm.ToString();
            txtInterestRate.Text = InterestRatePrcPerYear.ToString();
            txtMonthlyPayment.Text = MonthlyPayment.ToString();
            // Remove the constant from input text box that we are going to calc
            switch (selectedCalcTypeIndex)
            {
                case 0:
                    txtMonthlyPayment.Text = "";
                    break;
                case 1:
                    txtLoanAmount.Text = "";
                    break;
                case 2:
                    txtLoanTerm.Text = "";
                    break;
                case 3:
                    txtInterestRate.Text = "";
                    break;
            }

            btnCalculate.Enabled = calcInputStatus();
        }

        // This method will be started when calculation type is changed and is perfect moment to disable the calculable value text box to prevent user input
        private void cboCalculationType_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Update global var
            selectedCalcTypeIndex = cboCalculationType.SelectedIndex;
            // 
            txtMonthlyPayment.Enabled = true;
            txtLoanAmount.Enabled = true;
            txtLoanTerm.Enabled = true;
            txtInterestRate.Enabled = true;
            // Lets reset the text boxes on the form. For that we will simply call the same method that subscribed to btn event
            resetCalculator(btnReset, e);

            btnCalculate.Text = CalcTypes[selectedCalcTypeIndex];

            switch (selectedCalcTypeIndex)
            {
                case 0:
                    txtMonthlyPayment.Enabled = false;
                    cboLoanTermUnit.Enabled = true;
                    break;
                case 1:
                    txtLoanAmount.Enabled = false;
                    cboLoanTermUnit.Enabled = true;
                    break;
                case 2:
                    txtLoanTerm.Enabled = false;
                    cboLoanTermUnit.Enabled = false;
                    cboLoanTermUnit.SelectedIndex = 1;
                    break;
                case 3:
                    txtInterestRate.Enabled = false;
                    cboLoanTermUnit.Enabled = true;
                    break;
            }

        }
        
        // helper method to check if input char is allowed for decimal textboxes
        private void isNumericTextInput(object sender, KeyPressEventArgs e)
        {
            // only allow digits and decimal points
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }

            btnCalculate.Enabled = calcInputStatus();
        }

        // method for updating the calculation objects
        private bool calcInputStatus()
        {

            bool allInputState = false;

            bool isLoanAmountInputOk = false;
            bool ismonthlyPaymentInputOk = false;
            bool isloanTermInputOk = false;
            bool isinterestRateInputOk = false;

            double _loanAmount;
            double _monthlyPayment;
            int _loanTerm;
            double _interestRate;

            try
            {
                isLoanAmountInputOk = double.TryParse(txtLoanAmount.Text, out _loanAmount);
                isloanTermInputOk = int.TryParse(txtLoanTerm.Text, out _loanTerm);
                ismonthlyPaymentInputOk = double.TryParse(txtMonthlyPayment.Text, out _monthlyPayment);
                isinterestRateInputOk = double.TryParse(txtInterestRate.Text, out _interestRate);
            } finally {
                // Check which calculation is to be done
                switch (selectedCalcTypeIndex)
                {
                    // Payment Amount (€)
                    case 0:
                        {
                            if (isLoanAmountInputOk && isloanTermInputOk && isinterestRateInputOk)
                            {
                                allInputState = true;
                                // Update the values
                                monthlyPaymentCalculation.InterestRate = double.Parse(txtInterestRate.Text);
                                monthlyPaymentCalculation.LoanAmount = double.Parse(txtLoanAmount.Text);
                                monthlyPaymentCalculation.LoanTerm = int.Parse(txtLoanTerm.Text);
                            }
                            break;
                        }
                    // Loan Amount (€)
                    case 1:
                        {
                            if (ismonthlyPaymentInputOk && isloanTermInputOk && isinterestRateInputOk)
                            {
                                allInputState = true;
                                // Update the values
                                loanAmountCalculation.InterestRate = double.Parse(txtInterestRate.Text);
                                loanAmountCalculation.MonthlyPayment = double.Parse(txtMonthlyPayment.Text);
                                loanAmountCalculation.LoanTerm = int.Parse(txtLoanTerm.Text);
                            }
                            break;
                        }
                    // Loan Term
                    case 2:
                        {
                            if (isLoanAmountInputOk && ismonthlyPaymentInputOk && isinterestRateInputOk)
                            {
                                allInputState = true;
                                // Update the values
                                loanTermCalculation.InterestRate = double.Parse(txtInterestRate.Text);
                                loanTermCalculation.MonthlyPayment = double.Parse(txtMonthlyPayment.Text);
                                loanTermCalculation.LoanAmount = double.Parse(txtLoanAmount.Text);
                            }
                            break;
                        }
                    // Interest rate (%)
                    case 3:
                        {
                            if (isLoanAmountInputOk && isloanTermInputOk && ismonthlyPaymentInputOk)
                            {
                                allInputState = true;
                                // Update the values
                                interestRateCalculation.LoanTerm = int.Parse(txtLoanTerm.Text);
                                interestRateCalculation.MonthlyPayment = double.Parse(txtMonthlyPayment.Text);
                                interestRateCalculation.LoanAmount = double.Parse(txtLoanAmount.Text);
                            }
                            break;
                        }
                }
            }

            return allInputState;
        }

        private void isNumericTextInput(object sender, EventArgs e)
        {
            btnCalculate.Enabled = calcInputStatus();
        }

        private void cboLoanTermUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool isLoanTermMonthly = cboLoanTermUnit.SelectedIndex == 1 ? true : false;
            // Update all calculation
            monthlyPaymentCalculation.IsLoanTermMonthly = isLoanTermMonthly;
            loanAmountCalculation.IsLoanTermMonthly = isLoanTermMonthly;
            loanTermCalculation.IsLoanTermMonthly = isLoanTermMonthly;
            interestRateCalculation.IsLoanTermMonthly = isLoanTermMonthly;
        }
    }
}
