﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Calculator;

namespace appTest
{
    [TestClass]
    public class CalculationTest
    {
        [TestMethod]
        public void Calculate_LoanTerm()
        {
            var calc = new LoanTermCalculation();
            calc.MonthlyPayment = 200;
            calc.LoanAmount = 1000;
            calc.IsLoanTermMonthly = false;
            calc.InterestRate = 7;

            calc.Calculate();

            var actual = calc.Result();
            double expected = 6;

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Calculate_MonthlyPayment_WithLoanTermAnnual()
        {
            var calc = new MonthlyPaymentCalculation();
            calc.LoanAmount = 1000;
            calc.LoanTerm = 5;
            calc.IsLoanTermMonthly = false;
            calc.InterestRate = 7;

            calc.Calculate();

            var actual = calc.Result();
            double expected = 19.7;

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Calculate_LoanAmount_WithLoanTermAnnual()
        {
            var calc = new LoanAmountCalculation();
            calc.MonthlyPayment = 200;
            calc.LoanTerm = 5;
            calc.IsLoanTermMonthly = false;
            calc.InterestRate = 7;

            calc.Calculate();

            var actual = calc.Result();
            double expected = 10152.33;

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Calculate_InterestRate_WithLoanTermAnnual()
        {
            var calc = new InterestRateCalculation();
            calc.MonthlyPayment = 200;
            calc.LoanTerm = 5;
            calc.IsLoanTermMonthly = false;
            calc.LoanAmount = 1000;

            calc.Calculate();

            var actual = calc.Result();
            double expected = 239.98;

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Calculate_MonthlyPayment_WithLoanTermMonthly()
        {
            var calc = new MonthlyPaymentCalculation();
            calc.LoanAmount = 1000;
            calc.LoanTerm = 5;
            calc.IsLoanTermMonthly = true;
            calc.InterestRate = 7;

            calc.Calculate();

            var actual = calc.Result();
            double expected = 203.41;

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Calculate_LoanAmount_WithLoanTermMonthly()
        {
            var calc = new LoanAmountCalculation();
            calc.MonthlyPayment = 200;
            calc.LoanTerm = 5;
            calc.IsLoanTermMonthly = true;
            calc.InterestRate = 7;

            calc.Calculate();

            var actual = calc.Result();
            double expected = 983.26;

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void Calculate_InterestRate_WithLoanTermMonthly()
        {
            var calc = new InterestRateCalculation();
            calc.MonthlyPayment = 500;
            calc.LoanTerm = 24;
            calc.IsLoanTermMonthly = true;
            calc.LoanAmount = 10000;

            calc.Calculate();

            var actual = calc.Result();
            double expected = 18.12;

            Assert.AreEqual(expected, actual);
        }
    }
}
